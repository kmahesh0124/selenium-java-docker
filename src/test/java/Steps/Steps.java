package Steps;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
//import org.apache.commons.logging.Log;
//import java.awt.image.*;
//import java.util.ArrayList;
import org.junit.Assert;
//import org.junit.Test;
import org.testng.Reporter;
//import org.testng.Assert;
//import org.testng.annotations.*;
//import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;

//import org.openqa.selenium.Dimension;
//import java.awt.List;
import java.util.List;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.TakesScreenshot;
//import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.chrome.ChromeDriverService;
//import org.openqa.selenium.chrome.ChromeDriverService.Builder;
import org.openqa.selenium.chrome.ChromeOptions;
//import org.openqa.selenium.safari.SafariDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
//import jdk.internal.org.jline.utils.Log;
import cucumber.api.java.en.And;



//import org.openqa.selenium.remote.DesiredCapabilities;
/*import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.net.MalformedURLException;
import java.net.URL;*/
//import org.openqa.selenium.chrome.ChromeOptions;







public class Steps extends spreadsheet{
	
	private Scenario scenario;
	
	//TestBase testBase;
	
	//Logger log = LoggerHelper.getLogger(ServiceHooks.class);
	
	ChromeDriver driver;
	
	
	public void screenCapture() throws IOException{
		String timestamp = new SimpleDateFormat("_YYYYMMdd_hh.mm.ss_").format(new Date());
		TakesScreenshot ts = (TakesScreenshot)driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		File screenshotname = new File ("./target/screenshots/Screenshot" + timestamp + driver.getTitle()+ ".png ");
		FileUtils.copyFile(source, screenshotname);
		Reporter.log("<br><img src='"+screenshotname+"' height='400' width='400' /><b>");
	}
	
	@Before
	public void setup() throws Exception{
		//playback.startRecording("navigationTest");
		spreadsheet.readExcel();
	}
	
	@Before
    public void before(Scenario scenario) {
        this.scenario = scenario;
    }
	
	/*@Before
	public void setUp() throws MalformedURLException{
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		String Node = "http://10.197.4.87:4444/wd/hub";
		driver = new RemoteWebDriver(new URL(Node), capabilities);
		}*/
	
	@Before
	public void setUp() throws IOException{
		//String chromeDriverPath = "D:\\Apps\\chromedriver\\chromedriver.exe";
		String chromeDriverPath = "/usr/bin/chromedriver";
		System.setProperty("webdriver.chrome.driver", chromeDriverPath);
		System.setProperty("webdriver.chrome.silentOutput", "true");
		//DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		//capabilities.setCapability("something", true);
		ChromeOptions options = new ChromeOptions();
		//options.setBinary("/opt/google/chrome/google-chrome");
	
		options.addArguments("enable-automation");
		options.addArguments("--headless");
		options.addArguments("--disable-gpu");
		options.addArguments("--no-sandbox");
		options.addArguments("--disable-dev-shm-usage");
		options.addArguments("--log-level=3");
		options.addArguments("--silent");	
		options.addArguments("--force-device-scale-factor=1");
		options.addArguments("--aggressive-cache-discard"); 
		options.addArguments("--proxy-server='direct://'");
		options.addArguments("--proxy-bypass-list=*");
		options.addArguments("--disable-cache"); 
		options.addArguments("--disk-cache-size=0");
		options.addArguments("--disable-application-cache"); 
		options.addArguments("--disable-features=VizDisplayCompositor");
		options.addArguments("--disable-offline-load-stale-cache"); 
		options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(2000, TimeUnit.MILLISECONDS);
        //WebDriver driver = new ChromeDriver(options);
	}
	
	@Given("^I am at the login page$")
	public void i_am_at_the_login_page() throws Exception {
		//driver.navigate().to(URL);
		System.out.println(URL);
		driver.get(URL);
		//driver.manage().window().maximize();
		driver.manage().window().setSize(new Dimension(1280, 720));
		screenCapture();
		System.out.println("Successfully opened the login page");
		scenario.write("Successfully opened the login page\n");
	}
	
	@And("^Title of login page is Siebel Communications$")
	public void title_of_login_page_is_Siebel_Communications() throws Exception {
		driver.navigate().refresh();
		String title = driver.getTitle();
		System.out.println(title);
		Assert.assertEquals("Siebel Communications", title);
		if (driver.getTitle().equals("Siebel Communications")) {							
            System.out.println("We are at CRM landing Page");
            scenario.write("We are at CRM landing Page\n");
        } else {			
            System.out.println("We are NOT in CRM's Landing Page");	
            scenario.write("We are NOT in CRM's Landing Page\n");
        }
	}

	@When("^I fill my User ID and Password$")
	public void i_fill_my_User_ID_and_Password() throws Exception {
		String username = spreadsheet.username;
		String password = spreadsheet.password;
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		driver.findElement(By.name("SWEUserName")).sendKeys(username);
		driver.findElement(By.name("SWEPassword")).sendKeys(password);
		scenario.write("Entered username and password\n");
		try {
			screenCapture();
		} catch (IOException e) {
			System.out.println("Unable to login to application");
			scenario.write("Unable to login to application\n");
			e.printStackTrace();
		}
	}

	@Then("^I click Login Button$")
	public void i_click_Login_Button() {
		WebElement loginBtn = driver.findElement(By.id("s_swepi_22"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", loginBtn);
		scenario.write("clicked Login button\n");
	}

	@Then("^I should be directed to the landing page$")
	public void i_should_be_directed_to_the_landing_page() {
		String title = driver.getTitle();
		System.out.println("Home Page title ::" + title);
		Assert.assertEquals("Siebel Communications", title);
		if (title.equals("Siebel Communications")) {
			System.out.println("Test Passed: Login is successful");
			scenario.write("Test Passed: Login is successful\n");
		} else {
			System.out.println("Test Failed: Login has failed");
			scenario.write("Test Failed: Login has failed\n");
		}
		try {
			screenCapture();
		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("Unable to get page title");
		}
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	
	@Given("^I am on the landing page$")
	public void i_am_on_the_landing_page() {
		String title = driver.getTitle();
		System.out.println("Home Page title ::" + title);
		scenario.write("Home Page title ::" + title + "\n");
		Assert.assertEquals("Siebel Communications", title);
		if (title.equals("Siebel Communications")) {
			System.out.println("Test Passed: I am at the landing page");
			scenario.write("Test Passed: I am at the landing page\n");
		} else {
			System.out.println("Test Failed: I am not at the landing page");
			scenario.write("Test Failed: I am not at the landing page\n");
		}
		try {
			screenCapture();
		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("Unable to click on the Mpesa VAS tab");
		}
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	@Then("^I can click on the Home tab$")
	public void i_can_click_on_the_Home_tab() throws Exception {
		try {
		//WebDriverWait wait = new WebDriverWait(driver,30);
		WebElement homeBtn = driver.findElement(By.xpath("//a[text() = 'Home']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", homeBtn);
		
		/*WebElement salutation = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("span[class=siebui-salutation-title]"))));
		String goodNews = salutation.getText();
		
		if (goodNews.equals("My Homepage")) {
			System.out.println("I can click on the Home tab");
			driver.navigate().back();
		} else System.out.println("There is a system error in Home tab");*/
		
		/*try {
		WebElement AppletTitle = driver.findElement(By.cssSelector("td[class=AppletTitle]"));
		if(AppletTitle != null){
			System.out.println("AppletTitle Element is present");			
			if( AppletTitle.isDisplayed()){
				System.out.println("AppletTitle Element is visible");				
				String badNews = AppletTitle.getText();				
					if (badNews.equals("Error Message")) {
						System.out.println("There is a system error in Home tab");
						driver.navigate().back();
					} else System.out.println("I can click on the Home tab");
				}else{
				System.out.println("Error Message Element is Invisible");
				}			
			}else{
				System.out.println("Error Message Element is Absent");
			}
		} catch (Exception e1) {
			//logger.severe(e);
			System.out.println("Unable to click on the Home tab");
		}*/
		System.out.println("I can click on the Home tab");
		scenario.write("I can click on the Home tab\n");
		try {
			List<WebElement> AppletTitle = driver.findElements(By.cssSelector("td[class=AppletTitle]"));
				if(AppletTitle.size() != 0){
				 //System.out.println("Home VBC not OK: There is a system error in Home tab");
				 //driver.navigate().back();
				 
				 WebDriverWait wait = new WebDriverWait(driver,30);
					WebElement AppletTitle2 = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
					String badNews = AppletTitle2.getText();
					System.out.println(badNews);
					scenario.write(badNews + "\n");
					if (badNews.equals("Error Message")) {
						System.out.println("Home VBC not OK: There is a system error in Home tab");
						scenario.write(badNews + "\n");
						driver.navigate().back();
					} else {
						System.out.println("Home VBC OK: I can click on the Home tab");
						scenario.write("Home VBC OK: I can click on the Home tab\n");
					}
				}
				else{
				 System.out.println("Home VBC OK");
				 scenario.write("Home VBC OK\n");
				}
			} catch (Exception e1) {
			//logger.severe(e);
			System.out.println("Unable to click on the Home tab");
			scenario.write("Unable to click on the Home tab\n");
		}
		
		
		/*List<WebElement> AppletTitle = driver.findElements(By.cssSelector("td[class=AppletTitle]"));
		if(AppletTitle.size() != 0){
		 System.out.println("Element present");
		 
		 String badNews = AppletTitle.getText();		 
		 if (badNews.equals("Error Message")) {
				System.out.println("There is a system error in Home tab");
				driver.navigate().back();
			} else System.out.println("I can click on the Home tab");
		 
		}
		else{
		 System.out.println("Element not present. I can click on the Home tab");
		}*/
		
		
		//try {
		screenCapture();
		} catch (Exception e) {
			//e.printStackTrace();
			System.out.println("Unable to click on the Home tab");
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@Then("^I can click on the Subscribers tab$")
	public void i_can_click_on_the_Subscribers_tab() throws Exception {
		try {
		//WebDriverWait wait = new WebDriverWait(driver,30);
		WebElement homeBtn = driver.findElement(By.xpath("//a[text() = 'Subscribers']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", homeBtn);
		
		/*WebElement AppletTitle = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
		String badNews = AppletTitle.getText();
		
		if (badNews.equals("Error Message")) {
			System.out.println("There is a system error in Subscribers tab");
			driver.navigate().back();
		} else System.out.println("I can click on the Subscribers tab");*/
		
		System.out.println("I can click on the Subscribers tab");
		try {
			List<WebElement> AppletTitle = driver.findElements(By.cssSelector("td[class=AppletTitle]"));
				if(AppletTitle.size() != 0){
				 //System.out.println("Subscribers VBC not OK: There is a system error in Home tab");
				 //driver.navigate().back();
				 
				 	WebDriverWait wait = new WebDriverWait(driver,30);
					WebElement AppletTitle2 = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
					String badNews = AppletTitle2.getText();
					System.out.println(badNews);
					scenario.write(badNews +"\n");
					if (badNews.equals("Error Message")) {
						System.out.println("Subscribers VBC not OK: There is a system error in Subscribers tab");
						scenario.write(badNews +"\n");
						driver.navigate().back();
					} else {
						System.out.println("Subscribers VBC OK: I can click on the Subscribers tab");
						scenario.write("Subscribers VBC OK: I can click on the Subscribers tab\n");
					}
				 
				}
				else{
				 System.out.println("Subscribers VBC OK");
				 scenario.write("Subscribers VBC OK\n");
				}
			} catch (Exception e1) {
			//logger.severe(e);
			System.out.println("Unable to click on the Subscribers tab");
			scenario.write("Unable to click on the Subscribers tab\n");
		}
				
		//try {
		screenCapture();
		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("Unable to click on the Subscribers tab");
			scenario.write("Unable to click on the Subscribers tab\n");
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@Then("^I can click on the Customers tab$")
	public void i_can_click_on_the_Customers_tab() throws Exception {
		try {
		//WebDriverWait wait = new WebDriverWait(driver,30);
		WebElement homeBtn = driver.findElement(By.xpath("//a[text() = 'Customers']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", homeBtn);
		
		/*WebElement AppletTitle = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
		String badNews = AppletTitle.getText();
		
		if (badNews.equals("Error Message")) {
			System.out.println("There is a system error in Customers tab");
			driver.navigate().back();
		} else System.out.println("I can click on the Customers tab");*/
		
		System.out.println("I can click on the Customers tab");
		try {
			List<WebElement> AppletTitle = driver.findElements(By.cssSelector("td[class=AppletTitle]"));
				if(AppletTitle.size() != 0){
				 //System.out.println("Customers VBC not OK: There is a system error in Home tab");
				 //driver.navigate().back();
				 
					WebDriverWait wait = new WebDriverWait(driver,30);
					WebElement AppletTitle2 = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
					String badNews = AppletTitle2.getText();
					System.out.println(badNews);
					scenario.write(badNews + "\n");
					if (badNews.equals("Error Message")) {
						System.out.println("Customers VBC not OK: There is a system error in Customers tab");
						scenario.write("Customers VBC not OK: There is a system error in Customers tab\n");
						driver.navigate().back();
					} else {
						System.out.println("Customers VBC OK: I can click on the Customers tab");
						scenario.write("Customers VBC OK: I can click on the Customers tab\n");
					}
				 
				}
				else{
				 System.out.println("Customers VBC OK");
				 scenario.write("Customers VBC OK\n");
				}
			} catch (Exception e1) {
			//logger.severe(e);
			System.out.println("Unable to click on the Customers tab");
			scenario.write("Unable to click on the Customers tab\n");
		}
		
		//try {
		screenCapture();
		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("Unable to click on the Customers tab");
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@Then("^I can click on the Activities tab$")
	public void i_can_click_on_the_Activities_tab() throws Exception {
		try {
		//WebDriverWait wait = new WebDriverWait(driver,30);
		WebElement homeBtn = driver.findElement(By.xpath("//a[text() = 'Activities']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", homeBtn);
		
		/*WebElement AppletTitle = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
		String badNews = AppletTitle.getText();
		
		if (badNews.equals("Error Message")) {
			System.out.println("There is a system error in Activities tab");
			driver.navigate().back();
		} else System.out.println("I can click on the Activities tab");*/
		
		System.out.println("I can click on the Activities tab");
		try {
			List<WebElement> AppletTitle = driver.findElements(By.cssSelector("td[class=AppletTitle]"));
				if(AppletTitle.size() != 0){
				 //System.out.println("Activities VBC not OK: There is a system error in Home tab");
				 //driver.navigate().back();
				
					WebDriverWait wait = new WebDriverWait(driver,30);
					WebElement AppletTitle2 = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
					String badNews = AppletTitle2.getText();
					System.out.println(badNews);
					scenario.write(badNews + "\n");
					if (badNews.equals("Error Message")) {
						System.out.println("Activities VBC not OK: There is a system error in Activities tab");
						scenario.write("Activities VBC not OK: There is a system error in Activities tab\n");
						driver.navigate().back();
					} else {
						System.out.println("Activities VBC OK: I can click on the Activities tab");
						scenario.write("Activities VBC OK: I can click on the Activities tab\n");
					}
					
				}
				else{
				 System.out.println("Activities VBC OK");
				 scenario.write("Activities VBC OK\n");
				}
			} catch (Exception e1) {
			//logger.severe(e);
			System.out.println("Unable to click on the Activities tab");
			scenario.write("Unable to click on the Activities tab\n");
		}
		
		//try {
		screenCapture();
		} catch (Exception e) {
			//e.printStackTrace();
			System.out.println("Unable to click on the Activities tab");
			scenario.write("Unable to click on the Activities tab\n");
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@Then("^I can click on the Accounts tab$")
	public void i_can_click_on_the_Accounts_tab() throws Exception {
		try {
		//WebDriverWait wait = new WebDriverWait(driver,30);
		WebElement homeBtn = driver.findElement(By.xpath("//a[text() = 'Accounts']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", homeBtn);
		
		/*WebElement AppletTitle = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
		String badNews = AppletTitle.getText();
		
		if (badNews.equals("Error Message")) {
			System.out.println("There is a system error in Accounts tab");
			driver.navigate().back();
		} else System.out.println("I can click on the Accounts tab");*/
		
		System.out.println("I can click on the Accounts tab");
		try {
			List<WebElement> AppletTitle = driver.findElements(By.cssSelector("td[class=AppletTitle]"));
				if(AppletTitle.size() != 0){
				 //System.out.println("Accounts VBC not OK: There is a system error in Home tab");
				 //driver.navigate().back();
				 
					WebDriverWait wait = new WebDriverWait(driver,30);
					WebElement AppletTitle2 = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
					String badNews = AppletTitle2.getText();
					System.out.println(badNews);
					scenario.write(badNews + "\n");
					if (badNews.equals("Error Message")) {
						System.out.println("Accounts VBC not OK: There is a system error in Accounts tab");
						scenario.write("Accounts VBC not OK: There is a system error in Accounts tab\n");
						driver.navigate().back();
					} else {
						System.out.println("Accounts VBC OK: I can click on the Accounts tab");
						scenario.write("Accounts VBC OK: I can click on the Accounts tab\n");
					}
				 
				}
				else{
				 System.out.println("Accounts VBC OK");
				 scenario.write("Accounts VBC OK\n");
				}
			} catch (Exception e1) {
			//logger.severe(e);
			System.out.println("Unable to click on the Accounts tab");
			scenario.write("Unable to click on the Accounts tab\n");
		}
		
		//try {
		screenCapture();
		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("Unable to click on the Accounts tab");
			scenario.write("Unable to click on the Accounts tab\n");
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	/*@Then("^I can click on the Archived Data tab$")
	public void i_can_click_on_the_Archived_Data_tab() throws Exception {
		try {
		//WebDriverWait wait = new WebDriverWait(driver,30);	
		WebElement homeBtn = driver.findElement(By.xpath("//a[text() = 'Archived Data']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", homeBtn);
		
		/*WebElement AppletTitle = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
		String badNews = AppletTitle.getText();
		
		if (badNews.equals("Error Message")) {
			System.out.println("There is a system error in Archived Data tab");
			driver.navigate().back();
		} else System.out.println("I can click on the Archived Data tab");*/
		
		/*System.out.println("I can click on the Archived Data tab");
		try {
			List<WebElement> AppletTitle = driver.findElements(By.cssSelector("td[class=AppletTitle]"));
				if(AppletTitle.size() != 0){
				 System.out.println("Archived Data VBC not OK: There is a system error in Home tab");
				}
				else{
				 System.out.println("Archived Data VBC OK");
				}
			} catch (Exception e1) {
			//logger.severe(e);
			System.out.println("Unable to click on the Archived Data tab");
		}
			
		String timestamp = new SimpleDateFormat("YYYYMMdd_hh.mm.ss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot)driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		//try {
		FileUtils.copyFile(source, new File ("./target/screenshots/Screenshot" + timestamp + ".png "));
		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("Unable to click on the Archived Data tab");
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//driver.close();
	}*/
	
	@Then("^I can click on the Service Requests tab$")
	public void i_can_click_on_the_Service_Requests_tab() throws Exception {
		try {
		//WebDriverWait wait = new WebDriverWait(driver,30);
		WebElement homeBtn = driver.findElement(By.xpath("//a[text() = 'Service Requests']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", homeBtn);
		
		/*WebElement AppletTitle = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
		String badNews = AppletTitle.getText();
		
		if (badNews.equals("Error Message")) {
			System.out.println("There is a system error in Service Requests tab");
			driver.navigate().back();
		} else System.out.println("I can click on the Service Requests tab");*/
		
		System.out.println("I can click on the Service Requests tab");
		scenario.write("I can click on the Service Requests tab\n");
		try {
			List<WebElement> AppletTitle = driver.findElements(By.cssSelector("td[class=AppletTitle]"));
				if(AppletTitle.size() != 0){
				 //System.out.println("Service Requests Data VBC not OK: There is a system error in Home tab");
				 //driver.navigate().back();
				 
					WebDriverWait wait = new WebDriverWait(driver,30);
					WebElement AppletTitle2 = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
					String badNews = AppletTitle2.getText();
					System.out.println(badNews);
					scenario.write(badNews + "\n");
					if (badNews.equals("Error Message")) {
						System.out.println("Service Requests VBC not OK: There is a system error in Service Requests tab");
						scenario.write("Service Requests VBC not OK: There is a system error in Service Requests tab\n");
						driver.navigate().back();
					} else {
						System.out.println("Service Requests VBC OK: I can click on the Service Requests tab");
						scenario.write("Service Requests VBC not OK: There is a system error in Service Requests tab\n");
					}
				 
				}
				else{
				 System.out.println("Service Requests VBC OK");
				 scenario.write("Service Requests VBC OK\n");
				}
			} catch (Exception e1) {
			//logger.severe(e);
			System.out.println("Unable to click on the Service Requests tab");
			scenario.write("Unable to click on the Service Requests tab\n");
		}
		
		//try {
		screenCapture();
		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("Unable to click on the Service Requests tab");
			scenario.write("Unable to click on the Service Requests tab\n");
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@Then("^I can click on the Administration tab$")
	public void i_can_click_on_the_Administration_tab() throws Exception {
		try {
		//WebDriverWait wait = new WebDriverWait(driver,30);
		// WebElement loginBtn =
		// driver.findElement(By.xpath("//input[@type='submit']"));
		WebElement homeDropdownBtn = driver.findElement(By.id("j_s_sctrl_tabScreen"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", homeDropdownBtn);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Select dropdown = new Select(driver.findElement(By.id("j_s_sctrl_tabScreen")));
		dropdown.selectByVisibleText("Administration");
		
		/*WebElement AppletTitle = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
		String badNews = AppletTitle.getText();
		
		if (badNews.equals("Error Message")) {
			System.out.println("There is a system error in Administration tab");
			driver.navigate().back();
		} else System.out.println("I can click on the Administration tab");*/
		
		System.out.println("I can click on the Administration tab");
		scenario.write("I can click on the Administration tab\n");
		try {
			List<WebElement> AppletTitle = driver.findElements(By.cssSelector("td[class=AppletTitle]"));
				if(AppletTitle.size() != 0){
				 //System.out.println("Administration VBC not OK: There is a system error in Home tab");
				 //driver.navigate().back();
				 
					WebDriverWait wait = new WebDriverWait(driver,30);
					WebElement AppletTitle2 = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
					String badNews = AppletTitle2.getText();
					System.out.println(badNews);
					scenario.write(badNews + "\n");
					if (badNews.equals("Error Message")) {
						System.out.println("Administration VBC not OK: There is a system error in Administration tab");
						scenario.write("Administration VBC not OK: There is a system error in Administration tab\n");
						driver.navigate().back();
					} else {
						System.out.println("Administration VBC OK: I can click on the Administration tab");
						scenario.write("Administration VBC not OK: There is a system error in Administration tab\n");
					}
				 
				}
				else{
				 System.out.println("Administration VBC OK");
				 scenario.write("Administration VBC OK\n");
				}
			} catch (Exception e1) {
			//logger.severe(e);
			System.out.println("Unable to click on the Administration tab");
			scenario.write("Unable to click on the Administration tab\n");
		}
		
		//try {
		screenCapture();
		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("Unable to click on the Administration tab");
			scenario.write("Unable to click on the Administration tab\n");
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@Given("^User is at the Search tab$")
	public void user_is_at_the_Search_tab() throws Exception {
		try {
		WebDriverWait wait = new WebDriverWait(driver,30);
		WebElement homeBtn = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//a[text() = 'Search']"))));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", homeBtn);
		
		/*WebElement AppletTitle = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
		String badNews = AppletTitle.getText();
		
		if (badNews.equals("Error Message")) {
			System.out.println("There is a system error in Search tab");
			driver.navigate().back();
		} else System.out.println("I can click on the Search tab");*/
		
		System.out.println("I can click on the Search tab");
		scenario.write("I can click on the Search tab\n");
		try {
			List<WebElement> AppletTitle = driver.findElements(By.cssSelector("td[class=AppletTitle]"));
				if(AppletTitle.size() != 0){
				 //System.out.println("Search VBC not OK: There is a system error in Home tab");
				 //driver.navigate().back();
					
					WebElement AppletTitle2 = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
					String badNews = AppletTitle2.getText();
					System.out.println(badNews);
					scenario.write(badNews + "\n");
					if (badNews.equals("Error Message")) {
						System.out.println("Search VBC not OK: There is a system error in Search tab");
						scenario.write("Search VBC not OK: There is a system error in Search tab\n");
						driver.navigate().back();
					} else {
						System.out.println("Search VBC OK: I can click on the Search tab");
						scenario.write("Search VBC not OK: There is a system error in Search tab\n");
					}
										
				}
				else{
				 System.out.println("Search VBC OK");
				 scenario.write("Search VBC OK\n");
				}
			} catch (Exception e1) {
			//logger.severe(e);
			System.out.println("Unable to click on the Search tab");
			scenario.write("Unable to click on the Search tab\n");
		}
		
		//try {
		screenCapture();
		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("Unable to click on the Search tab");
			scenario.write("Unable to click on the Search tab\n");
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//Thread.sleep(10000);
		//driver.close();
	}
	
	//https://www.lambdatest.com/blog/selenium-waits-implicit-explicit-fluent-and-sleep/
	
	@When("^I query a customer MSISDN$")
	public void i_query_a_customer_MSISDN() throws Exception {
		int usernumber = spreadsheet.MSISDN;
		WebDriverWait wait = new WebDriverWait(driver,30);
		WebElement MSISDN = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("input[name=s_6_1_8_0]"))));
		//int usenumber=Integer.parseInt(usernumber);
		MSISDN.sendKeys("" + usernumber);
		WebElement homeBtn = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("button[id=s_6_1_6_0_Ctrl]"))));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		//js.executeScript("MSISDN.setAttribute('value', '"+usenumber+"')");
		js.executeScript("arguments[0].click();", homeBtn);
		System.out.println("I can query a customer MSISDN");
		scenario.write("I can query a customer MSISDN\n");
		try {
			screenCapture();
		} catch (IOException e) {
			e.printStackTrace();
		}
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
	}
	
	@And("^I am in the Subscribers page$")
	public void i_am_in_the_Subscribers_page() throws Exception {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		String appletTitle = driver.findElement(By.xpath("//div[contains(text(),'Subscribers')]")).getText();
		if (appletTitle.equals("Subscribers")) {						
            System.out.println("We are at Subscribers' landing Page");
            scenario.write("We are at Subscribers' landing Page\n");
            //System.out.println("I can click on the Subscribers tab");
    		try {
    			List<WebElement> AppletTitle = driver.findElements(By.cssSelector("td[class=AppletTitle]"));
    				if(AppletTitle.size() != 0){
    				 System.out.println("Subscribers VBC not OK: There is a system error in Home tab");
    				 scenario.write("Subscribers VBC not OK: There is a system error in Home tab\n");
    				 driver.navigate().back();
    				 
    				 WebDriverWait wait = new WebDriverWait(driver,30);
    				 WebElement AppletTitle2 = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
 					String badNews = AppletTitle2.getText();
 					System.out.println(badNews);
 					scenario.write(badNews + "\n");
 					if (badNews.equals("Error Message")) {
 						System.out.println("Subscribers VBC not OK: There is a system error in Subscribers tab");
 						scenario.write("Subscribers VBC not OK: There is a system error in Subscribers tab\n");
 						driver.navigate().back();
 					} else {
 						System.out.println("Subscribers VBC OK: I can click on the Subscribers tab");
 						scenario.write("Subscribers VBC OK: I can click on the Subscribers tab\n");
 					}
    				 
    				}
    				else{
    				 System.out.println("Subscribers VBC OK");
    				 scenario.write("Subscribers VBC OK\n");
    				}
    			} catch (Exception e1) {
    			//logger.severe(e);
    			System.out.println("Unable to click on the Subscribers tab");
    			scenario.write("Unable to click on the Subscribers tab\n");
    		}
            
        } else {			
            System.out.println("We are NOT in Subscribers' Landing Page");	
            scenario.write("We are NOT in Subscribers' Landing Page\n");
        }
		System.out.println("Test Passed");
		scenario.write("Test Passed\n");
		try {
			screenCapture();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Then("^I can click on the Contact Management tab$")
	public void i_can_click_on_the_Contact_Management_tab() throws Exception {
		try {
		//WebDriverWait wait = new WebDriverWait(driver,30);
		WebElement homeBtn = driver.findElement(By.xpath("//a[text() = 'Contact Management']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", homeBtn);
		js.executeScript("arguments[0].click();", homeBtn);
		
		/*WebElement AppletTitle = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
		String badNews = AppletTitle.getText();
		
		if (badNews.equals("Error Message")) {
			System.out.println("There is a system error in Contact Management tab");
			driver.navigate().back();
		} else System.out.println("I can click on the Contact Management tab");*/
		
		System.out.println("I can click on the Contact Management tab");
		scenario.write("\n");
		try {
			List<WebElement> AppletTitle = driver.findElements(By.cssSelector("td[class=AppletTitle]"));
				if(AppletTitle.size() != 0){
				 //System.out.println("Contact Management VBC not OK: There is a system error in Home tab");
				 //driver.navigate().back();
				 
					WebDriverWait wait = new WebDriverWait(driver,30);
   				 	WebElement AppletTitle2 = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
					String badNews = AppletTitle2.getText();
					System.out.println(badNews);
					scenario.write(badNews + "\n");
					if (badNews.equals("Error Message")) {
						System.out.println("Contact Management VBC not OK: There is a system error in Contact Management tab");
						scenario.write("Contact Management VBC not OK: There is a system error in Contact Management tab\n");
						driver.navigate().back();
					} else {
						System.out.println("Contact Management VBC OK: I can click on the Contact Management tab");
						scenario.write("Contact Management VBC OK: I can click on the Contact Management tab\n");
					}
				 
				}
				else{
				 System.out.println("Contact Management VBC OK");
				 scenario.write("Contact Management VBC OK\n");
				}
			} catch (Exception e1) {
			//logger.severe(e);
			e1.printStackTrace();
			//System.out.println("Unable to click on the Contact Management tab");
			//scenario.write("Unable to click on the Contact Management tab\n");
		}
		
		//try {
		screenCapture();
		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("Unable to click on the Contact Management tab");
			scenario.write("Unable to click on the Contact Management tab\n");
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//driver.close();
	}
	
	@Then("^I can click on the Mpesa VAS tab$")
	public void i_can_click_on_the_Mpesa_VAS_tab() throws Exception {
		try {
		WebElement homeBtn = driver.findElement(By.xpath("//a[text() = 'MPESA VAS']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", homeBtn);
		

		/*WebDriverWait wait = new WebDriverWait(driver,30);
		WebElement AppletTitle = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
		String badNews = AppletTitle.getText();
		
		if (badNews.equals("Error Message")) {
			System.out.println("There is a system error in Mpesa VAS tab");
			driver.navigate().back();
		} else System.out.println("I can click on the Mpesa VAS tab");*/
		
		System.out.println("I can click on the Mpesa VAS tab");
		scenario.write("I can click on the Mpesa VAS tab\n");
		try {
			List<WebElement> AppletTitle = driver.findElements(By.cssSelector("td[class=AppletTitle]"));
				if(AppletTitle.size() != 0){
				 //System.out.println("Mpesa VAS VBC not OK: There is a system error in Home tab");
				 //driver.navigate().back();
					
					WebDriverWait wait = new WebDriverWait(driver,30);
					WebElement AppletTitle2 = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
					String badNews = AppletTitle2.getText();
					System.out.println(badNews);
					scenario.write(badNews + "\n");
					if (badNews.equals("Error Message")) {
						System.out.println("Mpesa VAS VBC not OK: There is a system error in Mpesa VAS tab");
						scenario.write("Mpesa VAS VBC not OK: There is a system error in Mpesa VAS tab\n");
						driver.navigate().back();
					} else {
						System.out.println("Mpesa VAS VBC OK: I can click on the Mpesa VAS tab");
						scenario.write("Mpesa VAS VBC OK: I can click on the Mpesa VAS tab\n");
					}
					
				}
				else{
				 System.out.println("Mpesa VAS VBC OK");
				 scenario.write("Mpesa VAS VBC OK\n");
				}
			} catch (Exception e1) {
			//logger.severe(e);
			System.out.println("Unable to click on the Mpesa VAS tab");
			scenario.write("Unable to click on the Mpesa VAS tab\n");
		}
		
		//try {
		screenCapture();
		} catch (Exception e) {
			//e.printStackTrace();
			System.out.println("Unable to click on the Mpesa VAS tab");
			scenario.write("Unable to click on the Mpesa VAS tab\n");
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@Then("^I can click on the MSHWARI tab$")
	public void i_can_click_on_the_MSHWARI_tab() throws Exception {
		try {
		WebDriverWait wait = new WebDriverWait(driver,30);
		WebElement homeBtn = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//a[text() = 'M-SHWARI']"))));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", homeBtn);
		//Thread.sleep(500);
		js.executeScript("arguments[0].click();", homeBtn);
		
		/*WebElement AppletTitle = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
		String badNews = AppletTitle.getText();
		
		if (badNews.equals("Error Message")) {
			System.out.println("There is a system error in MSHWARI tab");
			driver.navigate().back();
		} else System.out.println("I can click on the MSHWARI tab");*/
		
		System.out.println("I can click on the MSHWARI tab");
		scenario.write("I can click on the MSHWARI tab\n");
		try {
			List<WebElement> AppletTitle = driver.findElements(By.cssSelector("td[class=AppletTitle]"));
				if(AppletTitle.size() != 0){
				 //System.out.println("MSHWARI VBC not OK: There is a system error in Home tab");
				 //driver.navigate().back();
				 
					WebElement AppletTitle2 = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
					String badNews = AppletTitle2.getText();
					System.out.println(badNews);
					
					if (badNews.equals("Error Message")) {
						System.out.println("MSHWARI VBC not OK: There is a system error in MSHWARI tab");
						scenario.write("MSHWARI VBC not OK: There is a system error in MSHWARI tab\n");
						driver.navigate().back();
					} else {
						System.out.println("MSHWARI VBC OK: I can click on the MSHWARI tab");
						scenario.write("MSHWARI VBC OK: I can click on the MSHWARI tab\n");
					}
				 
				}
				else{
				 System.out.println("MSHWARI VBC OK");
				 scenario.write("MSHWARI VBC OK\n");
				}
			} catch (Exception e1) {
			//logger.severe(e);
			System.out.println("Unable to click on the MSHWARI tab");
			scenario.write("Unable to click on the MSHWARI tab\n");
		}
		
		//try {
		screenCapture();
		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("Unable to click on the MSHWARI tab");
			scenario.write("Unable to click on the MSHWARI tab\n");
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@Then("^I can click EIR tab$")
	public void i_can_click_EIR_tab() throws Exception {
		try {
		//WebDriverWait wait = new WebDriverWait(driver,30);
		WebElement homeBtn = driver.findElement(By.xpath("//a[text() = 'EIR']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", homeBtn);
		//Thread.sleep(500);
		js.executeScript("arguments[0].click();", homeBtn);
		
		/*WebElement AppletTitle = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
		String badNews = AppletTitle.getText();
		
		if (badNews.equals("Error Message")) {
			System.out.println("There is a system error in EIR tab");
			driver.navigate().back();
		} else System.out.println("I can click on the EIR tab");*/
		
		System.out.println("I can click on the EIR tab");
		scenario.write("I can click on the EIR tab\n");
		try {
			List<WebElement> AppletTitle = driver.findElements(By.cssSelector("td[class=AppletTitle]"));
				if(AppletTitle.size() != 0){
				 //System.out.println("EIR VBC not OK: There is a system error in Home tab");
				 //driver.navigate().back();
				 
				 	WebDriverWait wait = new WebDriverWait(driver,30);
					WebElement AppletTitle2 = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
					String badNews = AppletTitle2.getText();
					System.out.println(badNews);
					scenario.write(badNews + "\n");
					if (badNews.equals("Error Message")) {
						System.out.println("EIR VBC not OK: There is a system error in EIR tab");
						scenario.write("EIR VBC not OK: There is a system error in EIR tab\n");
						driver.navigate().back();
					} else {
						System.out.println("EIR VBC OK: I can click on the EIR tab");
						scenario.write("EIR VBC OK: I can click on the EIR tab\n");
					}
					
					
				}
				else{
				 System.out.println("EIR VBC OK");
				 scenario.write("EIR VBC OK\n");
				}
			} catch (Exception e1) {
			//logger.severe(e);
			System.out.println("Unable to click on the EIR tab");
			scenario.write("Unable to click on the EIR tab\n");
		}
		
		//try {
		screenCapture();
		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("Unable to click on the EIR tab");
			scenario.write("Unable to click on the EIR tab\n");
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@Given("^I click on the Account hyperlink$")
	public void i_click_on_the_Account_hyperlink() {
		try {
		//WebDriverWait wait = new WebDriverWait(driver,30);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		//WebElement accountLink = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("a[id=s_7_1_27_0_mb]"))));
		//WebElement accountLink = driver.findElement(By.cssSelector("span[id=s_5_1_27_0]"));
		//WebElement gotoAccount = driver.findElement(By.cssSelector("a[id=s_7_1_27_0_mb]"));
		WebElement gotoAccount = driver.findElement(By.linkText("Account #:"));
		String link = gotoAccount.getAttribute("href");
		System.out.println(link);
		scenario.write(link);
		//js.executeScript("arguments[0].scrollIntoView(true);", accountLink);
		js.executeScript("arguments[0].click();", gotoAccount);		
		}
		catch (Exception e) {
			//e.printStackTrace();
			System.out.println("Unable to click on the Account hyperlink");
			scenario.write("Unable to click on the Account hyperlink\n");
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	
	/*@Then("^I can click on the Balance Monitoring tab$")
	public void i_can_click_on_the_Balance_Monitoring_tab() throws Exception {
		try {
		//WebDriverWait wait = new WebDriverWait(driver,30);
		WebElement homeBtn = driver.findElement(By.xpath("//a[text() = 'Balance Monitoring']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", homeBtn);
		js.executeScript("arguments[0].click();", homeBtn);
		
		/*WebElement AppletTitle = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
		String badNews = AppletTitle.getText();
		
		if (badNews.equals("Error Message")) {
			System.out.println("There is a system error in EIR tab");
			driver.navigate().back();
		} else System.out.println("I can click on the EIR tab");*/
		
		/*System.out.println("I can click on the Balance Monitoring tab");
		try {
			List<WebElement> AppletTitle = driver.findElements(By.cssSelector("td[class=AppletTitle]"));
				if(AppletTitle.size() != 0){
				 //System.out.println("Balance Monitoring VBC not OK: There is a system error in Balance Monitoring tab");
				 //driver.navigate().back();
				 
				 	WebDriverWait wait = new WebDriverWait(driver,30);
					WebElement AppletTitle2 = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
					String badNews = AppletTitle2.getText();
					System.out.println(badNews);
					
					if (badNews.equals("Error Message")) {
						System.out.println("Balance Monitoring VBC not OK: There is a system error in Balance Monitoring tab");
						driver.navigate().back();
					} else System.out.println("Balance Monitoring VBC OK: I can click on the Balance Monitoring tab");
				 
				}
				else{
				 System.out.println("Balance Monitoring VBC OK");
				}
			} catch (Exception e1) {
			//logger.severe(e);
			System.out.println("Unable to click on the Balance Monitoring tab");
		}
		
		String timestamp = new SimpleDateFormat("YYYYMMdd_hh.mm.ss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot)driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		//try {
			FileUtils.copyFile(source, new File ("./target/screenshots/Screenshot" + timestamp + ".png "));
		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("Unable to click on the Balance Monitoring tab");
			try {
				//WebDriverWait wait = new WebDriverWait(driver,30);
				// WebElement loginBtn =
				// driver.findElement(By.xpath("//input[@type='submit']"));
				WebElement homeDropdownBtn = driver.findElement(By.id("j_s_vctrl_div_tabScreen"));
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].click();", homeDropdownBtn);
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				Select dropdown = new Select(driver.findElement(By.id("j_s_sctrl_tabScreen")));
				dropdown.selectByVisibleText("Balance Monitoring");
				
				try {
					List<WebElement> AppletTitle = driver.findElements(By.cssSelector("td[class=AppletTitle]"));
						if(AppletTitle.size() != 0){
						 //System.out.println("Balance Monitoring VBC not OK: There is a system error in Balance Monitoring tab");
						 //driver.navigate().back();
						 
						 	WebDriverWait wait = new WebDriverWait(driver,30);
							WebElement AppletTitle2 = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
							String badNews = AppletTitle2.getText();
							System.out.println(badNews);
							
							if (badNews.equals("Error Message")) {
								System.out.println("Balance Monitoring VBC not OK: There is a system error in Balance Monitoring tab");
								driver.navigate().back();
							} else System.out.println("Balance Monitoring VBC OK: I can click on the Balance Monitoring tab");
						 
						}
						else{
						 System.out.println("Balance Monitoring VBC OK");
						}
					} catch (Exception e1) {
					//logger.severe(e);
					System.out.println("Unable to click on the Balance Monitoring tab");
				}
				
			}
		catch (Exception e2) {
			//e.printStackTrace();
			System.out.println("Unable to click on the Balance Monitoring tab");
		}
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}*/
	
	@Then("^I can click on the Credit Control tab$")
	public void i_can_click_on_the_Credit_Control_tab() throws Exception {
		try {
		//WebDriverWait wait = new WebDriverWait(driver,30);
		WebElement homeBtn = driver.findElement(By.xpath("//a[text() = 'Credit Control']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", homeBtn);
		//Thread.sleep(500);
		js.executeScript("arguments[0].click();", homeBtn);
		
		/*WebElement AppletTitle = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
		String badNews = AppletTitle.getText();
		
		if (badNews.equals("Error Message")) {
			System.out.println("There is a system error in Credit Control tab");
			driver.navigate().back();
		} else System.out.println("I can click on the Credit Control tab");*/
		
		System.out.println("I can click on the Credit Control tab");
		scenario.write("I can click on the Credit Control tab\n");
		try {
			List<WebElement> AppletTitle = driver.findElements(By.cssSelector("td[class=AppletTitle]"));
				if(AppletTitle.size() != 0){
				 //System.out.println("Credit Control VBC not OK: There is a system error in Home tab");
				 //driver.navigate().back();
					
					WebDriverWait wait = new WebDriverWait(driver,30);
					WebElement AppletTitle2 = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
					String badNews = AppletTitle2.getText();
					System.out.println(badNews);
					scenario.write(badNews + "\n");
					if (badNews.equals("Error Message")) {
						System.out.println("Credit Control VBC not OK: There is a system error in Credit Control tab");
						scenario.write("Credit Control VBC not OK: There is a system error in Credit Control tab\n");
						driver.navigate().back();
					} else {
						System.out.println("Credit Control VBC OK: I can click on the Credit Control tab");
						scenario.write("Credit Control VBC OK: I can click on the Credit Control tab\n");
					}
					
				}
				else{
				 System.out.println("Credit Control VBC OK");
				 scenario.write("Credit Control VBC OK\n");
				}
			} catch (Exception e1) {
			//logger.severe(e);
			System.out.println("Unable to click on the Credit Control tab");
			scenario.write("Unable to click on the Credit Control tab\n");
		}
		
		//try {
		screenCapture();
		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("Unable to click on the Credit Control tab");
			scenario.write("Unable to click on the Credit Control tab\n");
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@Then("^I can click on the Invoice Profile tab$")
	public void i_can_click_on_the_Invoice_Profile_tab() throws Exception {
		try {
		//WebDriverWait wait = new WebDriverWait(driver,30);
		WebElement homeBtn = driver.findElement(By.xpath("//a[text() = 'Invoice Profile']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", homeBtn);
		Thread.sleep(500);
		js.executeScript("arguments[0].click();", homeBtn);
		
		/*WebElement AppletTitle = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
		String badNews = AppletTitle.getText();
		
		if (badNews.equals("Error Message")) {
			System.out.println("There is a system error in Invoice Profile tab");
			driver.navigate().back();
		} else System.out.println("I can click on the Invoice Profile tab");*/
		
		System.out.println("I can click on the Invoice Profile tab");
		scenario.write("I can click on the Invoice Profile tab\n");
		try {
			List<WebElement> AppletTitle = driver.findElements(By.cssSelector("td[class=AppletTitle]"));
				if(AppletTitle.size() != 0){
				 //System.out.println("Invoice Profile VBC not OK: There is a system error in Home tab");
				 //driver.navigate().back();
					
					WebDriverWait wait = new WebDriverWait(driver,30);
					WebElement AppletTitle2 = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
					String badNews = AppletTitle2.getText();
					System.out.println(badNews);
					scenario.write(badNews + "\n");
					if (badNews.equals("Error Message")) {
						System.out.println("Invoice Profile VBC not OK: There is a system error in Invoice Profile tab");
						scenario.write("Invoice Profile VBC not OK: There is a system error in Invoice Profile tab\n");
						driver.navigate().back();
					} else {
						System.out.println("Invoice Profile VBC OK: I can click on the Invoice Profile tab");
						scenario.write("Invoice Profile VBC OK: I can click on the Invoice Profile tab\n");
					}
										
				}
				else{
				 System.out.println("Invoice Profile VBC OK");
				 scenario.write("Invoice Profile VBC OK\n");
				}
			} catch (Exception e1) {
			//logger.severe(e);
			System.out.println("Unable to click on the Invoice Profile tab");
			scenario.write("Unable to click on the Invoice Profile tab\n");
		}
		
		//try {
		screenCapture();
		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("Unable to click on the Invoice Profile tab");
			scenario.write("Unable to click on the Invoice Profile tab\n");
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@Then("^I can click on the Penalties tab$")
	public void i_can_click_on_the_Penalties_tab() throws Exception {
			Boolean isPresent = driver.findElements(By.xpath("//a[text() = 'Penalties']")).size() > 0;
			if(isPresent) {
				
				try {
					//WebDriverWait wait = new WebDriverWait(driver,30);
					WebElement homeBtn = driver.findElement(By.xpath("//a[text() = 'Penalties']"));
					JavascriptExecutor js = (JavascriptExecutor) driver;
					js.executeScript("arguments[0].scrollIntoView(true);", homeBtn);
					//Thread.sleep(500);
					js.executeScript("arguments[0].click();", homeBtn);
					
					/*WebElement AppletTitle = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
					String badNews = AppletTitle.getText();
					
					if (badNews.equals("Error Message")) {
						System.out.println("There is a system error in Penalties tab");
						driver.navigate().back();
					} else System.out.println("I can click on the Penalties tab");*/
					
					System.out.println("I can click on the Penalties tab");
					scenario.write("I can click on the Penalties tab\n");
					try {
						List<WebElement> AppletTitle = driver.findElements(By.cssSelector("td[class=AppletTitle]"));
							if(AppletTitle.size() != 0){
							 //System.out.println("Penalties VBC not OK: There is a system error in Home tab");
							 //driver.navigate().back();
								
								WebDriverWait wait = new WebDriverWait(driver,30);
								WebElement AppletTitle2 = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("td[class=AppletTitle]"))));
								String badNews = AppletTitle2.getText();
								System.out.println(badNews);
								scenario.write(badNews + "\n");
								if (badNews.equals("Error Message")) {
									System.out.println("Penalties VBC not OK: There is a system error in Penalties tab");
									scenario.write("Penalties VBC not OK: There is a system error in Penalties tab\n");
									driver.navigate().back();
								} else {
									System.out.println("Penalties VBC OK: I can click on the Penalties tab");
									scenario.write("Penalties VBC OK: I can click on the Penalties tab\n");
								}
								
							}
							else{
							 System.out.println("Penalties VBC OK");
							 scenario.write("Penalties VBC OK\n");
							}
						} catch (Exception e1) {
						//logger.severe(e);
						System.out.println("Unable to click on the Penalties tab");
						scenario.write("Unable to click on the Penalties tab\n");
					}
					screenCapture();
					/*String timestamp = new SimpleDateFormat("YYYYMMdd_hh.mm.ss").format(new Date());
					TakesScreenshot ts = (TakesScreenshot)driver;
					File source = ts.getScreenshotAs(OutputType.FILE);
					//try {
						File screenshotname = new File ("./target/screenshots/Screenshot" + timestamp + driver.getTitle()+ ".png ");
						FileUtils.copyFile(source, screenshotname);
						Reporter.log("<br><img src='"+screenshotname+"' height='400' width='400' /><b>");*/
						
					} catch (IOException e) {
						//e.printStackTrace();
						System.out.println("Unable to click on the Penalties tab");
						scenario.write("Unable to click on the Penalties tab\n");
						throw(e);
					}
				
			}else {
				SoftAssert softAssert = new SoftAssert();
				System.out.println("Penalties element not found");
				scenario.write("Penalties element not found\n");
				//SoftAssert.assertTrue("True", isPresent);
				//Assert.assertTrue(false);
				//Assert.assertTrue(isPresent);
				//softAssert.Assert.fail();
				///softassert.assertTrue("True",isPresent);
				//SoftAssert.assertTrue(isPresent,"True");
				softAssert.assertTrue(isPresent, "False");
				//softAssert.assertAll();
			}
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	}
	
	@Given("^I click on the logoff button$")
	public void i_click_on_the_logoff_button() {
		WebDriverWait wait = new WebDriverWait(driver,30);
		WebElement settings = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("li[id=tb_0]"))));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", settings);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		WebElement logoffBtn = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("div[id=tb_item_4]"))));
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].click();", logoffBtn);
	}
	
	@Then("^I am back on the landing page$")
	public void i_am_back_on_the_landing_page() throws Exception {
		//WebDriverWait wait = new WebDriverWait(driver,30);
		String title = driver.getTitle();
		Thread.sleep(1000);
		System.out.println(title);
		Assert.assertEquals("Siebel Communications", title);
		if (driver.getTitle().equals("Siebel Communications")) {					
            System.out.println("I am back at CRM landing Page");
            scenario.write("I am back at CRM landing Page\n");
        } else {
            System.out.println("I am NOT in CRM's Landing Page");
            scenario.write("I am NOT in CRM's Landing Page\n");
        }
	}
	
	/*@After("@tag:Login, @tag:Home_VBC, @tag:Query_Number, @tag:Query_Account, @tag:Logoff")
    public void killBrowser(Scenario scenario){
        if (scenario.isFailed()) {
         scenario.embed(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES), "image/png");
        }
        driver.close();
        driver.quit();
    }*/
	
	/*@After("Home_VBC")
    public void killBrowser(Scenario scenario){
        if (scenario.isFailed()) {
        	// Take a screenshot...
            final byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        // Take a screenshot...
         //scenario.embed(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES), "image/png");
      // embed it in the report.
         scenario.embed(screenshot, "image/png"); 
        }
        //driver.close();
        //driver.quit();
    }*/
	
	/*@After("@tag:Login")
	public void embed() throws IOException{
		try {
			scenario.write("See below error of screenshot\n");
			embedCapture();
		}catch (IOException e) {
			scenario.write("Error attaching screenshot to Report\n");
			System.out.println("Error attaching screenshot to Report");
		}
	}*/
	
	
	/*@After
	public void killBrowser(Scenario scenario) {
	    if (scenario.isFailed()) {
	      // Take a screenshot...
	      final byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
	   // embed it in the report.
	      scenario.embed(screenshot, "image/png"); 
	    }
	}*/
	
	@After
	public void endTest(Scenario scenario) throws Exception{
		if (scenario.isFailed()) {
						
			try {
				//Log.info(scenario.getName() + " is Failed");
				// Take a screenshot...
				final byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
				// embed it in the report
				scenario.embed(screenshot, "image/png");
				//Close browser and stop record
				//playback.stopRecording();
				driver.quit();
			} catch (Exception e) {
				//e.printStackTrace();
				System.out.println("Unable to get page title");
				//Close browser and stop record
				//playback.stopRecording();
				driver.quit();
			}
			
		}else {
			
			try {
				//Log.info(scenario.getName() + " is Passed");
				// Take a screenshot...
				scenario.embed(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES), "image/png");
				//Close browser and stop record
				//playback.stopRecording();
				driver.quit();
			} catch (Exception e) {
				e.printStackTrace();
				//Close browser and stop record
				//playback.stopRecording();
				driver.quit();
			}
			
		}
		
	}
	
	
	/*@After
	public void tearDown() throws Exception{
		//playback.stopRecording();
		//driver.close();
		driver.quit();
	}*/

}
