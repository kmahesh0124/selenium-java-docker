@AllFeatures
Feature: Login action, MyAccount and VBCs
	In order to use the system,
    one must register with the system 
    so that one gets authorized and may login

    
#Login background
Background: Verify Login Functionality
	Given I am at the login page
	And Title of login page is Siebel Communications
    When I fill my User ID and Password
    Then I click Login Button
    Then I should be directed to the landing page

@Home_VBC  	
Scenario: Verify user is able to click on the Home VBCs
	Given I am on the landing page
	Then I can click on the Home tab
	Then I can click on the Subscribers tab
	Then I can click on the Customers tab
	Then I can click on the Activities tab
	Then I can click on the Accounts tab
	#Then I can click on the Archived Data tab
	Then I can click on the Service Requests tab
	Then I can click on the Administration tab

@Query_Number
Scenario: User has to successfully query a customer number
	Given User is at the Search tab
	When I query a customer MSISDN
	And I am in the Subscribers page
	Then I can click on the Contact Management tab
	Then I can click on the Mpesa VAS tab
	Then I can click on the MSHWARI tab
	Then I can click EIR tab
	
@Query_Account
Scenario: User has to successfully query a customer account
	Given User is at the Search tab
	When I query a customer MSISDN
	And I am in the Subscribers page
	Given I click on the Account hyperlink
	#Then I am in the Accounts page
	#Then I can click on the Balance Monitoring tab
	Then I can click on the Credit Control tab
	Then I can click on the Invoice Profile tab
	Then I can click on the Penalties tab
	
@Logoff
Scenario: Verify Logoff Functionality
	Given I click on the logoff button
	Then I am back on the landing page